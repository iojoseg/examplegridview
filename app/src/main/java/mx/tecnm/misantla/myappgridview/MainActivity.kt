package mx.tecnm.misantla.myappgridview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.GridView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var paises = ArrayList<String>()

        paises.add("Mexico")
        paises.add("USA")
        paises.add("Honduras")
        paises.add("Argentina")

        var grid:GridView = findViewById(R.id.Grid)
        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,paises)
        grid.adapter = adaptador

        grid.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            Toast.makeText(this,paises.get(position), Toast.LENGTH_LONG).show()
        }
    }
}